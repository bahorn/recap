#!/usr/bin/env python3
"""
Web UI based of dash to do analysis.
"""
import json
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

from diff import USBDiff

app = dash.Dash(
    __name__,
    external_stylesheets=[dbc.themes.BOOTSTRAP]
)

files = []

def difference():
    """
    Return a component that compares the differences between two captures.
    """
    options = [{'label': file, 'value': file} for file in files]

    return html.Div(
        [
            html.Div(
                [
                    dbc.Label("File 1", html_for="file-1"),
                    dcc.Dropdown(
                        id='file-1',
                        options=options,
                        value=options[0]['value']
                    ),
                    dbc.Label("File 2", html_for="file-2"),
                    dcc.Dropdown(
                        id='file-2',
                        options=options,
                        value=options[0]['value']
                    ),
                ],
                className="mb-3"
            ),
            html.Div(id='difference-output-container')
        ]
    )


@app.callback(
    Output('difference-output-container', 'children'),
    Input('file-1', 'value'),
    Input('file-2', 'value')
)
def update_difference(file1, file2):
    """
    I'm sorry, this is a horrible function.

    I'll fix it later(TM)
    """
    res = []
    d = USBDiff()
    a = json.load(open(file1, 'r'))
    b = json.load(open(file2, 'r'))
    a_ = list(filter(lambda x: x['type'] != 'comment', a['data']))
    b_ = list(filter(lambda x: x['type'] != 'comment', b['data']))
    for idx, packet in enumerate(d.diff(a, b)):
        # just print the data differences right now.
        if 'data' not in packet or 'differences' not in packet['data']:
            continue

        differences = packet['data']['differences']
        a_d = a_[idx]['data']
        b_d = b_[idx]['data']
        p_a = [
            html.Span(a_d[i:i+2] + " ", className='packet_match')
            for i in range(0, len(a_d), 2)
        ]
        p_b = [
            html.Span(b_d[i:i+2] + " ", className='packet_match')
            for i in range(0, len(b_d), 2)
        ]
        for difference in differences:
            pos = difference['position']
            p_a[pos].className = 'packet_diff'
            p_b[pos].className = 'packet_diff'

        res.append(
            dbc.Card(
                [
                    dbc.CardHeader("Packet %i" % idx),
                    dbc.CardBody([
                        html.Div(p_a, className='packet_data'),
                        html.Hr(),
                        html.Div(p_b, className='packet_data')
                    ])
                ],
                className="mb-3"
            )
        )
    return html.Div(children=res)


def clustering():
    """
    Return a component that compares 
    """
    return html.Div()


def page():
    """
    The page we want to return.
    """
    return dbc.Container([
        html.H1(children='recap'),
        dbc.Card([
            dbc.CardHeader(
                dbc.Tabs(
                    id='actions',
                    children=[
                        dbc.Tab(
                            label='Diff',
                            tab_id='diff'
                        ),
                        dbc.Tab(
                            label='Cluster',
                            tab_id='cluster'
                        )
                    ],
                    active_tab='diff'
                )
            ),
            dbc.CardBody(
                html.P(id="card-content", className="card-text")
            )
        ])
    ])

@app.callback(
    Output("card-content", "children"), [Input("actions", "active_tab")]
)
def tab_content(active_tab):
    if active_tab == 'diff':
        return difference()
    elif active_tab == 'cluster':
        return clustering()
    return "This is tab {}".format(active_tab)

class WebUI:
    """
    WebUI. Construct it with a list of files to use for analysis.
    """
    def __init__(self, files_):
        global files
        files = files_
        app.layout = page()

    def start(self):
        """
        Start the UI.
        """
        app.run_server()


if __name__ == "__main__":
    pass
