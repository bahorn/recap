#!/usr/bin/env python3
"""
Novice friendly approach to discovering a USB devices vendor and product ID.

Just unplug the device, and replug in back in.
"""
import time
import usb.core


class FindDevice:
    """
    Find devices by looking at what is new when an device is plugged in.

    Gets the VID:PID.

    Also gets the bus, address, and interfaces so we can start doing a packet
    capture.
    """
    def __init__(self):
        self.known_devices = {}

    def first_pass(self):
        """
        First pass just involves finding the known devices.
        """
        self.known_devices = FindDevice.list_devices()

    def second_pass(self):
        """
        In the second pass, we look at all the devices and check if we've seen
        them in the first pass.

        If not, probably what we just plugged in so we report it.
        """
        new_devices = []
        for name, data in FindDevice.list_devices().items():
            if name in self.known_devices:
                continue
            new_devices.append((name, data))
        return new_devices

    @staticmethod
    def list_devices():
        """
        Return a list of all the devices plugged in.
        """
        res = {}
        for device in usb.core.find(find_all=1):
            # discovering the interfaces
            # commented out for now, but we'll want this to figure out which
            # interface to use for some devices.
            #
            # interfaces = []
            #for cfg in device:
            #    for interface in cfg.interfaces():
            #        print(interface)

            # now build the metadata object
            data = {
                'vendor_id': device.idVendor,
                'product_id': device.idProduct,
                'vendor_name': device.iManufacturer,
                'product_name': device.iProduct,
                'bus': device.bus,
                'address': device.address,
            }
            res['%04x:%04x' % (data['vendor_id'], data['product_id'])] = data
        return res


def find_device():
    """
    User friendly-ish example of the class.
    """
    finder = FindDevice()
    input("Unplug the device! [enter] ")
    finder.first_pass()
    input("Press enter when the device is plugged back in [enter] ")
    # do a quick sleep so the device has time to show up.
    time.sleep(1)
    new_devices = finder.second_pass()
    return new_devices


if __name__ == "__main__":
    print(find_device())
