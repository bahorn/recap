#!/usr/bin/env python3
"""
Capture packets through the shell script.
"""
import subprocess


class DeviceCapture:
    """
    Capture Packets sent from the host to the device.
    """
    def __init__(self, interface, address):
        self.interface = interface
        self.address = address
        self.script = './scripts/capture.sh'

    def capture(self, output_file, timeout=10):
        """
        Try to capture packets on the interface.
        Default timeout is 10 seconds.
        """
        # would have used pyshark, hit a bug where i wasn't getting any packets
        # in my output file, even if it was logged.
        # couldn't figure it, assume wasn't being flushed to disk or
        # something.
        cmd = [
            self.script,
            str(timeout),
            str(self.interface),
            str(self.address),
            output_file
        ]
        subprocess.run(cmd, check=True)


if __name__ == "__main__":
    d = DeviceCapture(3, 2)
    d.capture('/tmp/out.pcap')
