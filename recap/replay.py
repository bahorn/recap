#!/usr/bin/env python3
"""
Generates code based on a set of JSON files
"""
import json
from jinja2 import Environment, FileSystemLoader


class PythonGenerator:
    """
    Build a class that represents several captures.
    """
    def __init__(self, name, vid, pid, interfaces=[0]):
        self.name = name
        self.raw_methods = {}
        self.env = Environment(
            loader=FileSystemLoader('./templates')
        )
        self.env.trim_blocks = True
        self.env.lstrip_blocks= True
        self.vid = vid
        self.pid = pid
        self.interfaces = interfaces

    def add(self, file):
        """
        Add a new file to the class.
        """
        relevant = []
        with open(file, 'r') as f:
            j = json.load(f)
            for event in j['data']:
                if event['type'] == 'comment':
                    continue
                relevant.append(event)

        if not relevant:
            return
        
        name = file.split('/')[-1].split('.')[0]
        self.raw_methods[name] = relevant

    def build(self):
        """
        Generate the file based on provided information.
        """
        t = self.env.get_template('template.py')
        res = t.render(
            class_name=self.name,
            methods=self.raw_methods,
            pid=self.pid,
            vid=self.vid,
            interfaces=self.interfaces
        )
        return res

if __name__ == "__main__":
    g = PythonGenerator('Implementation', 0x1038, 0x150d, [0])
    g.add('../usbrply/data.json')
    #g.add('../usbrply/red.json')
    #g.add('../usbrply/green.json')
    #g.add('../usbrply/blue.json')
    g.build()
