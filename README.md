# recap

Tool to help generate code that controls USB device and do some analysis of the captures.

This is not perfect and it has limitations, but hopefully a good starting point!

## Setup

Currently Linux only, due to some dependencies.
Can work around this by doing the captures on a Linux host, and forwarding the USB device to a Windows VM to generate the packets.


Otherwise, standard virtualenv setup:
```
virtualenv -p python3 .venv
source .venv/bin/activate
pip install -r requirements.txt
```

You need to install usbrply separately as it doesn't seem to play nicely with the existing virtual environment.
If you have an issue, make sure you are using the project from git.


Beyond that:
* `gl_issue.sh` requires [jq](https://stedolan.github.io/jq/)
* `capture.sh` (used by the capture command) needs `tshark`, so `apt-get install tshark`

## Usage

```
$ python3 recap find-device
Unplug the device! [enter]: 
Plug the device back in! [enter]: 
Device found:
-> VID: 1038
-> PID: 150d
-> Bus: 3
-> Address: 35
$ python3 recap capture /tmp/red.pcap 3 35
Capturing on 'usbmon3'
16 
$ python3 recap capture /tmp/green.pcap 3 35
Capturing on 'usbmon3'
16 
$ python3 recap capture /tmp/blue.pcap 3 35
Capturing on 'usbmon3'
16
$ usbrply -j /tmp/red.pcap > /tmp/red.json
$ usbrply -j /tmp/green.pcap > /tmp/green.json
$ usbrply -j /tmp/blue.pcap > /tmp/blue.json
$ python3 recap generator QCKPrism 1038 150d /tmp/red.json /tmp/green.json /tmp/blue.json > sample/out.py
$ python3 sample/out.py
```

The final `sample/out.py` will cycle through all the recordings, as they are individual methods.

## Features

### Finding USB device IDs

This has a two pass process where the user unplugs, then replugs the device back in to discover the device!

### Automatic Packet Capture

Essentially the following is done:
```
tshark -i usbmon{BUS_ID} -Y "usb.device_address == {ADDRESS}"
```

Done with a shell script.

### Replay

Replays the packets, allowing control over the device!

Does some code generation and returns a class that can directly interface with the device based on packet captures!

### Analysis

#### Differences

There is currently a basic capture diff tool built in that might be useful.

```
$ python3 recap diff /tmp/red.json /tmp/blue.json
Packet 0 differs between the captures
- data : data difference
-- Pos: 4 
--- A: ff
--- B: 00
-- Pos: 5 
--- A: 00
--- B: 2f
-- Pos: 6 
--- A: 19
--- B: ff
-- Pos: 16 
--- A: ff
--- B: 00
-- Pos: 17 
--- A: 00
--- B: 2f
-- Pos: 18 
--- A: 19
--- B: ff
```

#### Clustering

You can attempt to cluster packets into classes by using the following:

```
$ python3 recap cluster-packets /tmp/red.json /tmp/blue.json /tmp/green.json --threshold 0.5 | jq
```

It returns the packets in a JSON object, where each class number is the key for a list of packets. Packets are in the same format as usbrply.


#### Search

You can search for byte strings by:
```
$ python3 recap search "FF00" /tmp/red.json
/tmp/red.json
-> Packet 1 at positions: 4, 16
```

More complex problems can be done by working off passing a function and window size to the `PacketSearch` class.

## Web UI

There is also a web UI built with `dash` that provides a somewhat nicer interface to some of the tools.

Start it with:
```
$ python3 recap web /tmp/red.json /tmp/green.json /tmp/blue.json
```

*NB just the diff right now*

## Notes

* When a packet number is displayed, it is zero-indexed. All comments in the usbrply format are ignored in the count.

## Libraries

Built using several libraries!

* [PyUSB](https://github.com/pyusb/pyusb)
* [usbrply](https://github.com/JohnDMcMaster/usbrply)

## License

MIT for my stuff.

ISC for `templates/template.py` as it's derived from usbrply. See the header.
