#!/usr/bin/env python3
"""
Finds the difference between two packet captures in the JSON format.

Assumes they are the same length and otherwise pretty similar.
Just meant to catch small differences like color, or the wrong method being
used.
"""
import json
import binascii


class USBDiff:
    """
    Class to diff USB packet captures in the form provided by usbrply
    """
    # Keys to ignore, as they will either always differ or just not be
    # interesting.
    IGNORE = ['t', 'id', 'comments', 'sec', 'usec']

    def __init__(self):
        pass

    def diff(self, a, b):
        """
        Diff two packet captures.
        """
        # allow either files or passing dicts
        a_ = a
        if not isinstance(a, dict):
            a_ = json.load(open(a, 'r'))
        b_ = b
        if not isinstance(b, dict):
            b_ = json.load(open(b, 'r'))
        
        a_ = filter(lambda x: x['type'] != 'comment', a_['data'])
        b_ = filter(lambda x: x['type'] != 'comment', b_['data'])

        results = []
        for i, j in zip(a_, b_):
            results.append(self.diff_dict(i, j))

        return results

    def diff_dict(self, a, b):
        """
        Find the difference between two dicts in this format.
        """
        keys = filter(
            lambda x: x not in self.IGNORE,
            set(list(a.keys()) + list(b.keys()))
        )

        diff = {}

        for key in keys:
            # first check if there are any differences at all, and if not just
            # skip this key.
            if a[key] == b[key]:
                continue

            # recursively traverse, though it shouldn't ever go beyond one
            # layer
            if isinstance(a[key], dict):
                res = self.diff_dict(a[key], b[key])
                if res != {}:
                    diff[key] = res
                continue

            if key == 'data':
                res = self.diff_data(a[key], b[key])
                if res != {}:
                    diff[key] = res
                continue

            # else just log the difference i guess.
            # this will happen
            diff[key] = {
                'info': 'Generic difference',
                'a': a[key],
                'b': b[key]
            }

        return diff

    def diff_data(self, a, b):
        """
        Finds the difference between the bytes in a packet.
        """
        diff = []
        pair = zip(binascii.unhexlify(a), binascii.unhexlify(b))
        for idx, (i, j) in enumerate(pair):
            if i != j:
                diff.append({
                    'position': idx,
                    'a': '%02x' % i,
                    'b':  '%02x' % j
                })
        if len(diff) < 1:
            return {}

        return {'info': 'data difference', 'differences': diff}

    def print_packet_diff(self, differences):
        # these functions are terrible and need some improvement, but they do
        # work i think.
        self.print_dict_diff(differences)

    def print_dict_diff(self, differences):
        for key, value in differences.items():
            if value == {}:
                continue
            if key == 'data':
                self.print_data_diff(key, value)
            elif value['info'] == 'Generic difference':
                self.print_generic_diff(key, value)
            else:
                self.print_dict_diff(value)

    def print_data_diff(self, key, value):
        print('- %s : %s' % (key, value['info']))
        for diff in value['differences']:
            print('-- Pos: %s \n--- A: %s\n--- B: %s' % (
                diff['position'], diff['a'], diff['b']
            ))

    def print_generic_diff(key, value):
        print('- %s : %s' % (key, value['info']))
        print('-- A: %s' % value['a'])
        print('-- B: %s' % value['b'])


if __name__ == "__main__":
    d = USBDiff()

    print(d.diff('/tmp/red.json', '/tmp/green.json'))
