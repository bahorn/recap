#!/usr/bin/env python3
"""
Entrypoint to the CLI of recap.
"""
import binascii
import json
import click
import time
from find_device import FindDevice
from capture import DeviceCapture
from replay import PythonGenerator
from diff import USBDiff
from similarity import cluster
from search import find_sequence
from ui import WebUI

@click.group()
def cli():
    pass

@cli.command()
def find_device():
    """
    Discover a devices VID, PID, Bus and Address.
    """
    finder = FindDevice()
    click.prompt('Unplug the device!', hide_input=True, default="enter")
    finder.first_pass()
    click.prompt('Plug the device back in!', hide_input=True, default="enter")
    time.sleep(1)
    new_devices = finder.second_pass()

    for device, data in new_devices:
        print('Device found:')
        print('-> VID: %04x' % data['vendor_id'])
        print('-> PID: %04x' % data['product_id'])
        print('-> Bus: %i' % data['bus'])
        print('-> Address: %i' % data['address'])


@cli.command()
@click.argument('output_file')
@click.argument('bus')
@click.argument('address')
@click.option('--time', default=10, help='How long to capture packets for')
def capture(output_file, bus, address, time):
    """
    Capture packets on the bus for a certain amount of time.
    """
    d = DeviceCapture(bus, address)
    d.capture(output_file, timeout=time)


@cli.command()
@click.argument('class_name')
@click.argument('vendor_id')
@click.argument('product_id')
@click.option('--interface', default=(0,), multiple=True)
@click.argument('files', nargs=-1)
def generator(class_name, vendor_id, product_id, interface, files):
    """
    Generate a class to control a device based off captures.
    """
    if len(files) < 1:
        return

    g = PythonGenerator(class_name, int(vendor_id, 16), int(product_id, 16),
                        interface)
    for file in files:
        g.add(file)

    print(g.build())


@cli.command()
@click.argument('capture_1')
@click.argument('capture_2')
def diff(capture_1, capture_2):
    """
    Find the differences between two captures.
    """
    differ = USBDiff()
    res = differ.diff(capture_1, capture_2)
    for idx, packet in enumerate(res):
        if packet == {}:
            continue

        print('Packet %i differs between the captures' % idx)
        differ.print_packet_diff(packet)


@cli.command()
@click.argument('files', nargs=-1)
@click.option('--threshold', default=1.0)
@click.option(
    '--linkage-criteria',
    type=click.Choice(['single', 'complete', 'average', 'weighted', 'centroid',
                       'median', 'ward']),
    default='average'
)
def cluster_packets(files, threshold, linkage_criteria):
    """
    Cluster packets based on similarity.
    """
    if len(files) < 1:
        return

    packets = []
    for file in files:
        with open(file, 'r') as f:
            j = json.load(f)
            packets += j['data']

    packets = list(filter(lambda x: x['type'] != 'comment', packets))

    print(json.dumps(cluster(packets, threshold, lc=linkage_criteria)))

@cli.command()
@click.argument('sequence')
@click.argument('files', nargs=-1)
def search(sequence, files):
    """
    Search packet captures for a sequence of bytes.
    """
    if len(files) < 1:
        return

    s = find_sequence(binascii.unhexlify(sequence))

    for file in files:
        packets = []
        with open(file, 'r') as f:
            j = json.load(f)
            packets = j['data']

        packets = list(filter(lambda x: x['type'] != 'comment', packets))

        found = []
        for idx, packet in enumerate(packets):
            found.append((idx, s.search(packet)))

        if found != []:
            print(file)
            for idx, packet in found:
                if packet == []:
                    continue
                print(
                    '-> Packet %i at positions: %s' % (
                        idx,
                        ', '.join(map(str, packet))
                    )
                )


@cli.command()
@click.argument('files', nargs=-1)
def web(files):
    """
    Spin up a web UI to do analysis.
    """
    if len(files) < 1:
        return

    ui = WebUI(files)
    ui.start()


if __name__ == "__main__":
    cli()
