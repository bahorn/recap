#!/usr/bin/env python3
"""
Functions to help find similarities between packets in our captures.
"""
import binascii
import os
from itertools import combinations
from scipy.spatial.distance import hamming, pdist
from scipy.cluster.hierarchy import linkage, fcluster


def length(packet):
    """
    Get the length from the packet
    """
    return packet['submit']['urb']['data_length']


def pad(data, padding):
    """
    Add bytes until we have a byte array of the correct length.

    Using random bytes are added to keep bytes from matching by accident.
    """
    return [i for i in data + os.urandom(padding - len(data))]


def weights(length):
    """
    We bias weight to prioritize the first few bytes, over the later bytes.
    """
    count = 4
    if length < count:
        return [1.0 for i in range(length)]

    # quite a steep drop off, may want to experiment further.
    return [1.0 for i in range(count)] + \
        [1 / (i + 1) for i in range(length - count)]


def distance(a, b):
    """
    Computes the distance between two packets.
    """
    a_d = binascii.unhexlify(a['data'])
    b_d = binascii.unhexlify(b['data'])
    padding = max(len(a_d), len(b_d))
    # currently just do byte differences for hamming.
    # tbh we might want to look at bit level, at least in some cases as some
    # protocols likely encode information packed into a single byte.

    # basically as our strings get longer, the differences to the end make less
    # of a difference.
    # considering as we want to cluster similar sizes together, makes sense to
    # counteract this slightly.
    length_weight = 1.025**(abs(len(a_d) - len(b_d)))

    return length_weight * hamming(
        pad(a_d, padding),
        pad(b_d, padding),
        weights(padding)
    )

def cluster(packets, threshold=1.0, lc='average'):
    """
    Cluster the packets using a hierarchical method.
    """
    indexes = range(len(packets))
    
    values = []
    for i, j in combinations(indexes, 2):
        values.append(distance(packets[i], packets[j]))

    links = linkage(
        values,
        lc
    )

    res = {}
    for idx, c in enumerate(fcluster(links, t=threshold, criterion='distance')):
        if c not in res:
            res[int(c)] = []
        res[int(c)].append(packets[idx])
 
    return res

if __name__ == "__main__":
    pass
