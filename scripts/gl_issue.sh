#!/bin/sh
# Download all the pcaps in a gitlab issue.
ISSUE_URL=$1
# hopefully this doesn't have a horrible edge case...
ISSUE=`echo $ISSUE_URL | grep -o "[123456789]*$"`
REPO=`echo $ISSUE_URL | grep -oP "(?<=//gitlab.com/).*(?=/-/)"`

URL=`echo $ISSUE_URL | grep -oP ".*(?=/-/)"`

# Now we need the project id for that repo
PROJECT_ID=$(curl https://gitlab.com/api/v4/projects/`echo $REPO | sed "s/\//%2f/g"` 2>/dev/null | jq .id)

curl https://gitlab.com/api/v4/projects/$PROJECT_ID/issues/$ISSUE 2>/dev/null \
    | jq .description -r \
    | grep "^\[.*\]\(.*\)" \
    | grep  "\/.*/.*/.*.\(pcap\|pcapng\)" -o \
    | xargs -I {} -n 1 echo $URL{} \
    | xargs wget -w 1
