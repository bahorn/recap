#!/usr/bin/env python3
"""
Util functions
"""
from enum import Enum

class bmRequestTypeDataPhaseTransferDirection(Enum):
    HOST_TO_DEVICE = 0
    DEVICE_TO_HOST = 1


class bmRequestTypeType(Enum):
    STANDARD = 0
    CLASS = 1
    VENDOR = 2
    RESERVED = 3


class bmRequestTypeRecipient(Enum):
    DEVICE = 0
    INTERFACE = 1
    ENDPOINT = 2
    OTHER = 3
    RESERVED = 4
    # yolo hope we don't get anything between 5-31

def decode_bmRequestType(value):
    """
    bitmap format, so just want to read what is in it
    """
    # https://www.beyondlogic.org/usbnutshell/usb6.shtml
    r_direction = bmRequestTypeDataPhaseTransferDirection(
        int('{0:08b}'.format(value)[0:1], 2)
    )
    r_type = bmRequestTypeType(
        int('{0:08b}'.format(value)[1:3], 2)
    )
    r_recipient = bmRequestTypeRecipient(
        int('{0:08b}'.format(value)[3:8], 2)
    )

    return (r_direction, r_type, r_recipient)
