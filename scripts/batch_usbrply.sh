#!/bin/sh

INDIR=$1

find $INDIR -name \*.pcapng | 
    xargs -I {} sh -c "usbrply -j {} > {}.json"
