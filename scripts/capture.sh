#!/bin/sh
# annoyingly, i wanted to do this in python but for some reason it wouldn't
# always save if i didn't get enough packets....

TMPFILE=`mktemp`
BUS=$2
ADDRESS=$3
OUTPUT=$4

FILTER="usb.device_address == $ADDRESS" # and usb.src == host" # and not usb.setup.bRequest == 6"

tshark -i usbmon$BUS -a duration:$1 -w $TMPFILE
tshark -r $TMPFILE -w $OUTPUT -Y "$FILTER" -F pcapng

rm $TMPFILE
