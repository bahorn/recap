#!/usr/bin/env python3
"""
Search the data component of a packet using a windowed search method.
"""
import binascii


class PacketSearch:
    """
    Searches the data component by iteratively calling a function that takes a
    defined window of data.
    """
    def __init__(self, func, window):
        self.func = func
        self.window = window

    def search(self, packet):
        """
        Attempt to search the packets
        """
        results = []
        if packet['type'] == 'comment':
            return results

        data = binascii.unhexlify(packet['data'])
        if len(data) < self.window:
            return results

        for i in range(0, len(data) - self.window + 1):
            if self.func(data[i:i + self.window]):
                results.append(i)

        return results


def find_sequence(sequence):
    """
    Returns a PacketSearch instance that finds the sequence of bytes.
    """
    window = len(sequence)

    def fun(data):
        for i in range(window):
            if data[i] != sequence[i]:
                return False
        return True

    return PacketSearch(fun, window)


if __name__ == "__main__":
    s = find_sequence(b'\xaa\xbb\xcc\xdd')
    assert s.search({'type': 'controlWrite', 'data': 'AABBCCDD'}) == [0]
    assert s.search({'type': 'controlWrite', 'data': 'AABBCCEE'}) == []
    assert s.search({'type': 'controlWrite', 'data': 'AABBCCDDAABBCCDD'}) \
        == [0, 4]
    assert s.search({'type': 'controlWrite', 'data': 'AAAABBCCDD'}) \
        == [1]
    assert s.search({'type': 'controlWrite', 'data': 'DDAABBCCDDDD'}) \
        == [1]
