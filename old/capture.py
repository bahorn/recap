#!/usr/bin/env python3
"""
Capture Packets from a usb interface and address.
"""
import pyshark
import tempfile
import os

class DeviceCapture:
    """
    Capture Packets sent from the host to the device.
    """
    def __init__(self, interface, address):
        self.interface = interface
        # goal of this filter is to only show packets going from the host to
        # the device, and avoid the irrelevant descriptor sharing.
        self.filter = ' and '.join([
            'usb.device_address == %i' % address,
            'usb.src == host',
            'not usb.setup.bRequest == 6'
        ])

    def capture(self, output_file, timeout=10):
        """
        Try to capture packets on the interface.
        Default timeout is 10 seconds.
        """
        # So tshark, which pyshark uses internally, doesn't support display
        # filters when capturing packets.
        # So, first we have have to capture everything and then filter it to get
        # a useful save file.

        # deprecated function, but we don't care about security for this.
        tmpfile = tempfile.mktemp()

        live = pyshark.LiveCapture(
            interface=self.interface,
            output_file=tmpfile
        )
        live.sniff(timeout=timeout)
        live.close()
        
        last = pyshark.FileCapture(
            tmpfile,
            #display_filter=self.filter,
            output_file=output_file,
            keep_packets=True
        )

        for packet in last:
            print(packet)

        last.close()
        # and delete the temp file.
        os.remove(tmpfile)

if __name__ == "__main__":
    d = DeviceCapture('usbmon3', 2)
    print(d.capture('/tmp/out.pcap'))
